#include <Servo.h>
Servo Motor;

const int in1 = 12;
int x = 0; //idle
int sense1 = A0;
int sense2 = A1;
int sense3 = A2;
int sense4 = A3;


int ledPin = 13;      // select the pin for thwe LED
int sensorValue = 0;  // variable to store the value coming from the sensor


//C20 --> 
char floorReached = 0;
int floorState = 0;



void setup() {
 Motor.attach(12);
 Serial.begin(9600);
 pinMode(ledPin, OUTPUT);
 bool leaving = false;

  //C20
 pinMode(19, INPUT);
 pinMode(18, OUTPUT);
 Serial.begin(9600);
 Serial1.begin(4800);
}

/*
 * states are:
 * 0 = Motor 0 is stopped
 *     Motor 1 is moving from stop to full forward
 *
 * 1 = Motor 0 is full forward
 *     Motor 1 is moving from full forward to stop
 *
 * 2 = Motor 0 is stopped
 *     Motor 1 is moving from stop to full reverse
 *
 * 3 = Motor 0 is full reverse
 *     Motor 1 is moving from full reverse to stop
 */
 
void loop() {

  if (leaving == true) {
    delay(1000);
    leaving = false;
  }

  
 Sensors();

 //serial
 if (Serial1.available()){
    floorState = Serial1.read();
    Serial.write(Serial1.read());
 }
    
 if (floorReached) {
   Serial1.write(floorReached);
   leaving = true;
   floorReached = 0;
 }
}

void Sensors(){
  // Loops through all the elevator sensors.
 
  if(analogRead(sense1)  < 50){
    Serial.print("Level 1");
    MotorControl(0);
    floorReached = 1;
  } 


  else if(analogRead(sense2)  < 50){
    Serial.print("Level 2");
    MotorControl(0);
    floorReached = 1;
  } 


  else if(analogRead(sense3)  < 50){
    Serial.print("Level 3");
    MotorControl(0);
    floorReached = 1;
  } 

  else if(analogRead(sense4)  < 50){
    Serial.print("Level 4");
    MotorControl(0);
    floorReached = 1;
  } 

  else{
    MotorControl(floorState);
  }
 }
  



void MotorControl(int x) {

  
  switch(x) {
    
    //Emergency Stop:
    case -1:
      Motor.write(90);
      Serial.print("Case -1 --> EMERGENCY STOP");
      break;
    
    
    
    //Idle:
    case 0:
     Motor.write(90);
     Serial.print("Case 0 --> IDLE");
      break;


    //Ascend:
    case 1:
      Motor.write(180);
      Serial.print("Case 1 --> ASCEND /n");
      break;
    
    
    //Descend:
    case 2:
      Motor.write(0);
      Serial.print("Case 2 --> DESCEND /n");
      break;
  } 
}
