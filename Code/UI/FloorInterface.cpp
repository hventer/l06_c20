#include "Arduino.h"
#include "FloorInterface.hpp"

FloorInterface::FloorInterface(char pinButton, char pinLED, char floor) {
    //pins
    this->pinButton = pinButton;
    this->pinLED = pinLED;
    this->floor = floor;

    //button states
    this->curButtonState = 0;
    this->preButtonState = 0;

    this->active = 0;
}

void FloorInterface::init() {
  //pin modes
    pinMode(pinLED, OUTPUT);
    pinMode(pinButton, INPUT_PULLUP);

    //set both to low initially
    digitalWrite(pinButton, HIGH);
    digitalWrite(pinLED, LOW);
}

char FloorInterface::getLevel() {
  return this->floor;
}

void FloorInterface::checkPressed() {
    preButtonState = curButtonState;
    curButtonState = digitalRead(pinButton);

      //if any button pressed do action
  if (preButtonState == 1 && curButtonState == 0) {
    activateLED();
    this->active = 1;
  }
}

void FloorInterface::activateLED() {
    digitalWrite(pinLED, HIGH);
}

void FloorInterface::deactivateLED() {
    digitalWrite(pinLED, LOW);
}

void FloorInterface::deactivate() {
  deactivateLED();
  this->active = 0;
}

bool FloorInterface::isActive() {
  if(this->active) {
    return true;
  }
  else {
    return false;
  }
}
