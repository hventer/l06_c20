#include "FloorInterface.hpp"
#include "EmergencyInterface.hpp"
#include <SoftwareSerial.h>

#define PIN_BUTTON_UP 6
#define PIN_LED_UP A2
#define PIN_BUTTON_DOWN 5
#define PIN_LED_DOWN A1
#define PIN_BUTTON_EMERGENCY 4
#define PIN_LED_EMERGENCY A0
#define RX 2
#define TX 3
#define DATA_RATE 4800

FloorInterface down(PIN_BUTTON_DOWN, PIN_LED_DOWN, 1);
FloorInterface up(PIN_BUTTON_UP, PIN_LED_UP, 2);
EmergencyInterface em(PIN_BUTTON_EMERGENCY, PIN_LED_EMERGENCY);
SoftwareSerial megaSerial(RX, TX);

//0 means doing something
//1 means levelReached
char megaState = 0;

void setup() {
  pinMode(RX, INPUT);
  pinMode(TX, OUTPUT);
  Serial.begin(9600);
  // set the data rate for the SoftwareSerial port
  megaSerial.begin(DATA_RATE);

  down.init();
  up.init();
  em.init();
}

void loop() {
  if(megaState) {
    down.deactivate();
    up.deactivate();
    
    if(!up.isActive())
      down.checkPressed();
    
    if(!down.isActive())
      up.checkPressed();
    
    if(down.isActive()) {
      megaSerial.write(down.getLevel());
      megaState = 0;
    }
    else if(up.isActive()) {
        megaSerial.write(up.getLevel());
        megaState = 0;
      }
   }
  

  // read what the MEGA has sent
  if(megaSerial.available()) {
    if(megaSerial.read() == '1') {
      Serial.println(megaSerial.read());
      megaState = 1;
    }
    

  }
 
  if(em.checkPressed()) {
    megaSerial.write(-1);
    down.deactivate();
    up.deactivate();
  }
}
