void ButtonPressCheck() {
  //updates pre and cur button presses
 // Serial.println(" UNO: ButtonPressCheck()");
  DownButtonPre = DownButtonCur;
  DownButtonCur = digitalRead(DownButton);

  UpButtonPre = UpButtonCur;
  UpButtonCur = digitalRead(UpButton);

  StopButtonPre = StopButtonCur;
  StopButtonCur = digitalRead(StopButton);

  //if any button pressed do action
  if (DownButtonPre == false && DownButtonCur == true && Action == "Null") {
    Serial.println(" UNO: Down Button Pressed");
    Action = DownAction;
    digitalWrite(DownLED, HIGH);
    Serial.write("DownAction#");
  }
  if (UpButtonPre == false && UpButtonCur == true && Action == "Null") {
    Serial.println(" UNO: Up Button Pressed");
    Action = UpAction;
    digitalWrite(UpLED, HIGH);
    Serial.write("UpAction#");
  }
  if (StopButtonPre == false && StopButtonCur == true && Action != StopAction) {
    Serial.println(" UNO: Stop Button Pressed");
    Action = StopAction;
    digitalWrite(DownLED, LOW);
    digitalWrite(UpLED, LOW);
    digitalWrite(StopLED, HIGH);
    Serial.write("StopAction#");
  }

}
