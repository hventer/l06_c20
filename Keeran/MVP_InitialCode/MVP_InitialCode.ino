int DownLED = 2;
int DownButton = 3;
int UpLED = 4;
int UpButton = 5;
int StopLED = 6;
int StopButton = 7;

/*
   Switch Problems
   These boolean values are used to prevent switches problems caused
   by the holding of a button, when a button is pressed the arduino will continue to read the pin value as fast as it can which is faster than out hand can react
   so by using two values we can wait for when the previous vlue was 0 and the current value is 1 that means the button was then pressed, the code will then set the previous =current
   and then pre =1, cur =1 we know the button is being held nd for the arduino to do nothing, also pre=1, cur=0, the button has been released

   Summary
   Previous = 0   Current = 1   Action = Button Pressed
   Previous = 1   Current = 1   Action = Button Held
   Previous = 1   Current = 0   Action = Button Released
*/
boolean DownButtonPre = false;
boolean DownButtonCur = false;
String DownAction = "DownAction";
boolean UpButtonPre = false;
boolean UpButtonCur = false;
String UpAction = "UpAction";
boolean StopButtonPre = false;
boolean StopButtonCur = false;
String StopAction = "StopAction";

String Action = "Null";

void setup() {
  // put your setup code here, to run once:


  Serial.begin(9600);
  Serial.println(" UNO: UNO MVP StartUp");


  /*
     Setting Up Arduino Pins to be either an input or an output
     pinMode(pin, mode)
     Pin = Arduino Pin
     mode = What you wish the pin to be used for
          Available Modes >> INPUT, OUTPUT, INPUT_PULLUP
  */
  pinMode(DownLED, OUTPUT); //Down LED
  pinMode(DownButton, INPUT); //Down Button
  pinMode(UpLED, OUTPUT); //Up LED
  pinMode(UpButton, INPUT); // Up Button
  pinMode(StopLED, OUTPUT); // Stop LED
  pinMode(StopButton, INPUT); //Stop Button


  /*
     Setting up The Pins default Value
     digitalWrite(pin, value)
     Pin =Arduino Pin
     Value = Desired Value of the pin
     value can either be high or low(on digital pins)
     low = ground
     high = 5V (or 3.3 on certain boards)
  */
  digitalWrite(DownLED, LOW); //Down LED
  digitalWrite(DownButton, LOW); //Down Button
  digitalWrite(UpLED, LOW); //Up LED
  digitalWrite(UpButton, LOW); // Up Button
  digitalWrite(StopLED, LOW); // Stop LED
  digitalWrite(StopButton, LOW); //Stop Button

}

void loop() {
  // put your main code here, to run repeatedly:
  ButtonPressCheck();


}

void serialEvent() {
  //Action =  Serial.readStringUntil('#');
  //Serial.println(Action);
  //Serial.flush();
  
}
