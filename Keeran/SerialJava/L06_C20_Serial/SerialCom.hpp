/*
 * set Serial Monitor to 115200 Baud to view logging
 * Uno Send and receive does not work due to calling of softserial issues
 */

#ifndef SerialCom_hpp
#define SerialCom_hpp
#include "Arduino.h"
#include <SoftwareSerial.h>

//Pin Allocation For Each Board

class SoftwareSerial;

class SerialCom {

  public:
    int RX;
    int TX;
    int AgreedValue = 0;
    String BoardType;
    long ArduinoComBaud = 38400;
    long PCBuad = 115200;
    boolean Logging = true;
    String LoggingDivider = "------------------------------------------";


    init();
    SerialCom();
    void sendMessage(int Message);
    void receiveMessage();
    void setAgreedValue(int value);
    int getAgreedValue();
    void logging(boolean logger) ;


    SoftwareSerial &SoftSerial;

};

#endif
