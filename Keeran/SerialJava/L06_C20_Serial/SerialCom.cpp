
#include "Arduino.h"
#include "SerialCom.hpp"
#include <SoftwareSerial.h>



//initialiser sets the pins that will be used on the arduino
SerialCom::SerialCom() {

}

SerialCom::init() {
  logging(true);
#ifdef ARDUINO_AVR_UNO
  this->RX = 2;
  this->TX = 3;
  this->BoardType = "ARDUINO_AVR_UNO";
  SoftSerial = SoftwareSerial(RX, TX);
  SoftSerial.begin(ArduinoComBaud);
#endif

#ifdef ARDUINO_AVR_MEGA2560
  this->BoardType = "ARDUINO_AVR_MEGA2560";
  Serial1.begin(ArduinoComBaud);
#endif

  if (this->Logging) {
    //Printing Board And File Information
    Serial.println("Current Board: "); Serial.println(this->BoardType);
    Serial.print("Sketch:   ");   Serial.println(__FILE__);
    Serial.print("Uploaded: ");   Serial.println(__DATE__);
    Serial.println(this->LoggingDivider);
    Serial.println("Serial Initialised");
#ifdef ARDUINO_AVR_UNO
    Serial.print("RX Pin: ");    Serial.println(this->RX);
    Serial.print("TX Pin: ");    Serial.println(this->TX);
#endif

#ifdef ARDUINO_AVR_MEGA2560
    Serial.println("Serial1 Connection");
#endif

    Serial.println(this->LoggingDivider);
  }

}

void SerialCom::sendMessage(int Message) {
#ifdef ARDUINO_AVR_UNO
  if (Message == -1) {
    SoftSerial.println('E');
  }
  if (Message == 0) {
    SoftSerial.println('a');
  }
  if (Message == 1) {
    SoftSerial.println('b');
  }
  if (Message == 2) {
    SoftSerial.println('c');
  }
  if (Message == 9) {
    SoftSerial.println('H');
  }

#endif

#ifdef ARDUINO_AVR_MEGA2560
  if (Message == -1) {
    Serial1.println('E');
  }
  if (Message == 0) {
    Serial1.println('a');
  }
  if (Message == 1) {
    Serial1.println('b');
  }
  if (Message == 2) {
    Serial1.println('c');
  }
#endif

  if (Logging) {
    Serial.print("Integer Message Sent: "); Serial.println(Message);
    Serial.println(LoggingDivider);
  }
}

void SerialCom::receiveMessage() {
#ifdef ARDUINO_AVR_UNO
  if (SoftSerial.available() > 1) {
    char val = SoftSerial.read();
    if (val == 'E') {
      AgreedValue = -1;
    }
    if (val == 'a') {
      AgreedValue = 0;

    }
    if (val == 'b') {
      AgreedValue = 1;
    }
    if (val == 'c') {
      AgreedValue = 2;
    }
    if (Logging) {
      Serial.print("Message received: "); Serial.println(AgreedValue);
      Serial.println(LoggingDivider);
    }
  }
#endif

#ifdef ARDUINO_AVR_MEGA2560
  if (Serial1.available() > 1) {
    char val = Serial1.read();
    if (val == 'E') {
      this->AgreedValue = -1;
    }
    if (val == 'a') {
      this->AgreedValue = 0;

    }
    if (val == 'b') {
      this->AgreedValue = 1;
    }
    if (val == 'c') {
      this->AgreedValue = 2;
    }

    if (Logging) {
      Serial.print("Message received: "); Serial.println(this->AgreedValue);
      Serial.println(this->LoggingDivider);
    }
  }
#endif
}

void SerialCom::setAgreedValue(int value) {

  if (value == -1 || value == 0 || value == 1 || value == 2) {
    this->AgreedValue = value;
  } else {
    this->AgreedValue = -1;
  }

  this->sendMessage(this->AgreedValue);

  if (Logging) {
    Serial.print("Agreed Value Set To: "); Serial.println(AgreedValue);
    Serial.println(LoggingDivider);
  }
}

int SerialCom::getAgreedValue() {
  return this->AgreedValue;

  if (Logging) {
    Serial.print("Agreed Value Retreived: "); Serial.println(AgreedValue);
    Serial.println(LoggingDivider);
  }
}




/*
   logging(boolean logger)
   This function is designed to allow data logging for fault finding,
   if logger=true it will allow for all other functions to print key information
   if logger=false this information will not be printed and therefore will not be
   included into the final code saving space
*/
void SerialCom::logging(boolean logger) {
  Serial.begin(this->PCBuad);
  this->Logging = logger;
  Serial.print("Logging: "); Serial.println(this->Logging);
  Serial.println(this->LoggingDivider);
}
