#include <SoftwareSerial.h>
int test = 0;
//Pin Allocation
#ifdef ARDUINO_AVR_UNO
#define RX 2
#define TX 3
#define BoardType "ARDUINO_AVR_UNO"
SoftwareSerial SoftSerial(RX, TX);
#endif

#ifdef ARDUINO_AVR_MEGA2560
#define RX 2
#define TX 3
#define BoardType "ARDUINO_AVR_MEGA2560"
#endif

// Serial Connection Baud Rate between Arduinos
long ArduinoComBaud = 9600;
//Serial Connection Baud Rate Between PC and Connected Arduino
long PCBuad = 115200;

//AgreedValue is value shared between C20 and C10 for designated instructions
int AgreedValue = 0;

//Logging to print statements to serial monitor between PC and Arduino
boolean Logging = false;
String LoggingDivider = "------------------------------------------";

//SoftSerial


void setup() {
   logging(true);

#ifdef ARDUINO_AVR_UNO
  SoftSerial.begin(ArduinoComBaud);
  SoftSerial.flush();
#endif

#ifdef ARDUINO_AVR_MEGA2560
  Serial1.begin(ArduinoComBaud);
  Serial1.flush();
#endif

  if (Logging) {
    //Printing Board And File Information
    Serial.println("Current Board: "); Serial.println(BoardType);
    Serial.print("Sketch:   ");   Serial.println(__FILE__);
    Serial.print("Uploaded: ");   Serial.println(__DATE__);
    Serial.println(LoggingDivider);
    Serial.println("Serial Initialised");
    Serial.print("RX Pin: ");    Serial.println(RX);
    Serial.print("TX Pin: ");    Serial.println(TX);
    Serial.println(LoggingDivider);
  }


}

void loop() {
  // put your main code here, to run repeatedly:
 receiveMessage();
  //sendMessage(-1);
  //delay(1000);
}

void setAgreedValue(int value) {
  switch (value) {
    case -1 : AgreedValue = value; // Emergency Stop
    case 0 : AgreedValue = value; // Idle
    case 1 : AgreedValue = value; // Descend
    case 2 : AgreedValue = value; // Ascend
    default: AgreedValue = -1; // if error occurs emergency stop
  }
  sendMessage(AgreedValue);

  if (Logging) {
    Serial.print("Agreed Value Set To: "); Serial.println(AgreedValue);
    Serial.println(LoggingDivider);
  }
}

int getAgreedValue() {
  return AgreedValue;
  if (Logging) {
    Serial.print("Agreed Value Retreived: "); Serial.println(AgreedValue);
    Serial.println(LoggingDivider);
  }
}

void sendMessage(int Message) {
#ifdef ARDUINO_AVR_UNO
  if (Message == -1) {
    SoftSerial.println('E');
  }
  if (Message == 0) {
    SoftSerial.println('a');
  }
  if (Message == 1) {
    SoftSerial.println('b');
  }
  if (Message == 2) {
    SoftSerial.println('c');
  }

#endif

#ifdef ARDUINO_AVR_MEGA2560
  if (Message == -1) {
    Serial1.println('E');
  }
  if (Message == 0) {
    Serial1.println('a');
  }
  if (Message == 1) {
    Serial1.println('b');
  }
  if (Message == 2) {
    Serial1.println('c');
  }
#endif

  if (Logging) {
    Serial.print("Integer Message Sent: "); Serial.println(Message);
    Serial.println(LoggingDivider);
  }
}

void receiveMessage() {

#ifdef ARDUINO_AVR_UNO
  if (SoftSerial.available() > 1) {
    char val = SoftSerial.read();
    if (val == 'E') {
      AgreedValue = -1;
    }
    if (val == 'a') {
      AgreedValue = 0;

    }
    if (val == 'b') {
      AgreedValue = 1;
    }
    if (val == 'c') {
      AgreedValue = 2;
    }

    if (Logging) {
      Serial.print("Message received: "); Serial.println(AgreedValue);
      Serial.println(LoggingDivider);
    }
  }
#endif

#ifdef ARDUINO_AVR_MEGA2560
  if (Serial1.available() > 0) {
    char val = Serial1.read();
    if (val == 'E') {
      AgreedValue = -1;
    }
    if (val == 'a') {
      AgreedValue = 0;

    }
    if (val == 'b') {
      AgreedValue = 1;
    }
    if (val == 'c') {
      AgreedValue = 2;
    }

    if (Logging) {
      Serial.print("Message received: "); Serial.println(AgreedValue);
      Serial.println(LoggingDivider);
    }
  }
#endif
}


/*
   logging(boolean logger)
   This function is designed to allow data logging for fault finding,
   if logger=true it will allow for all other functions to print key information
   if logger=false this information will not be printed and therefore will not be
   included into the final code saving space
*/
void logging(boolean logger) {
  Serial.begin(PCBuad);
  Serial.flush();
  Logging = logger;
  Serial.print("Logging: "); Serial.println(Logging);
  Serial.println(LoggingDivider);
}
