/*
   set Serial Monitor to 115200 Baud to view logging
   Uno Send and receive does not work due to calling of softserial issues
*/

#ifndef SerialCom_hpp
#define SerialCom_hpp
#include "Arduino.h"

//Pin Allocation For Each Board

class SerialCom {
  public:
    void init();
    SerialCom();
    void sendMessage(int Message);
    void receiveMessage();
    void setAgreedValue(int value);
    int getAgreedValue();
  private:
    String LoggingDivider = "------------------------------------------";
    long ArduinoComBaud = 9600;
    long PCBuad = 115200;
    int RX;
    int TX;
    int AgreedValue = 0;
int Emergency = -1;
int Idle = 53;
int Floor5 = A0;
int Floor4 = A1;
int Floor3 = A2;
int Floor2 = A3;
int Floor1 = A4;
};

#endif
