
#include "Arduino.h"
#include "SerialCom.hpp"

//initialiser sets the pins that will be used on the arduino
SerialCom::SerialCom() {
}

void SerialCom::init() {
  //Printing Board And File Information
  Serial.begin(PCBuad);
  Serial1.begin(ArduinoComBaud);
  Serial.println("Current Board: "); Serial.println("ARDUINO_AVR_MEGA2560");
  Serial.print("Sketch:   ");   Serial.println(__FILE__);
  Serial.print("Uploaded: ");   Serial.println(__DATE__);
  Serial.println(this->LoggingDivider);
  Serial.println("Serial Initialised");
  Serial.println("Serial1 Connection");
  Serial.println(this->LoggingDivider);
}

//if the message is an agreed value then it can and will be be sent otherwise error message and continue
void SerialCom::sendMessage(int Message) {
  Serial.print("Message To Send: ");
  Serial.println(Message);
  
  if (Message == this->Emergency) {
    Serial1.println('E');
    Serial.println("Message Sent: Emergency");
  } else if (Message == this->Idle) {
    char temp = Idle;
    Serial1.println(temp);
    Serial.println("Message Sent: Idle");
  } else if (Message == this->Floor5) {
    char temp = Floor5;
    Serial1.println(temp);
    Serial.println("Message Sent: Floor5");
  } else if (Message == this->Floor4) {
    char temp = Floor4;
    Serial1.println(temp);
    Serial.println("Message Sent: Floor4");
  } else if (Message == this->Floor3) {
    char temp = Floor3;
    Serial1.println(temp);
    Serial.println("Message Sent: Floor3");
  } else if (Message == this->Floor2) {
    char temp = Floor2;
    Serial1.println(temp);
    Serial.println("Message Sent: Floor2");
  } else if (Message == this->Floor1) {
    char temp = Floor1;
    Serial1.println(temp);
    Serial.println("Message Sent: Floor1");
  } else {
    Serial.println("SendMessage Value Is Not An Agreed Value And Has Not Been Sent");
  }

  Serial.println(LoggingDivider);

}

//if the incoming message is an agreed value set it to the agreedValue(destination) to be used
void SerialCom::receiveMessage() {
  if (Serial1.available() > 1) {
    char MessageChar = Serial1.read();
    int MessageInt = MessageChar + 2; //dont know why but it subtracts two when either converting or sending

    //if the message received is already its current state then ignore as it is spam
    /* this part is faulty, was designed to check that the message was an agreed valie, if it wasntitwas to return
      if (MessageInt == this->AgreedValue || MessageChar != 'E' || MessageInt != this->Idle || MessageInt != this->Floor5 || MessageInt != this->Floor4 || MessageInt != this->Floor3 || MessageInt != this->Floor2 || MessageInt != this->Floor1) {
      return;
      }
    */
    if (MessageChar == 'E') {
      this->AgreedValue = -1;
      Serial.println("Message Received: Emergency");
      Serial.println(LoggingDivider);
    }
    if (MessageInt == this->Idle) {
      this->AgreedValue = MessageInt;
      Serial.println("Message Received: Idle");
      Serial.println(LoggingDivider);
    }
    if (MessageInt == this->Floor5) {
      this->AgreedValue = MessageInt;
      Serial.println("Message Received: Floor5");
      Serial.println(LoggingDivider);
    }
    if (MessageInt == 59) {
      this->AgreedValue = this->Floor4;
      Serial.println("Message Received: Floor4");
      Serial.println(AgreedValue);
      Serial.println(LoggingDivider);
    }
    if (MessageInt == this->Floor3) {
      this->AgreedValue = MessageInt;
      Serial.println("Message Received: Floor3");
      Serial.println(AgreedValue);
      Serial.println(LoggingDivider);
    }
    if (MessageInt == this->Floor2) {
      this->AgreedValue = this->Floor2;
      Serial.println("Message Received: Floor2");
      Serial.println(AgreedValue);
      Serial.println(LoggingDivider);
    }
    if (MessageInt == this->Floor1) {
      this->AgreedValue = MessageInt;
      Serial.println("Message Received: Floor1");
      Serial.println(AgreedValue);
      Serial.println(LoggingDivider);
    }
  }
}

//if the value is an agreed value set it to the agreedValue(destination) to be used and sent otherwise error(Emergency)
void SerialCom::setAgreedValue(int value) {
  if (value == this->Emergency || value == this->Idle || value == this->Floor5 || value == this->Floor4 || value == this->Floor3 || value == this->Floor2 || value == this->Floor1) {
    this->AgreedValue = value;
  } else {
    this->AgreedValue = -1;
  }
  Serial.print("Agreed Value Set To: "); Serial.println(AgreedValue);
  Serial.println(LoggingDivider);
  this->sendMessage(this->AgreedValue);
}

//Gets the agreed Value
int SerialCom::getAgreedValue() {
  return this->AgreedValue;
  Serial.print("Agreed Value Retreived");
  Serial.println(LoggingDivider);
}


/*
   logging(boolean logger)
   This function is designed to allow data logging for fault finding,
   if logger=true it will allow for all other functions to print key information
   if logger=false this information will not be printed and therefore will not be
   included into the final code saving space
*/
