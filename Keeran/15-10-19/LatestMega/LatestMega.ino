/*
  Agreed Values, based on Arduino Mega Sesor Values
  Emergency = -1;
  Idle = 0
  Floor 5 = A4 = 58
  Floor 4 = A3 = 57
  Floor 3 = A2 = 56
  Floor 2 = A1 = 55
  Floor 1 = A0 = 54
*/
//Libraries
#include <Servo.h>
#include "SerialCom.hpp"
SerialCom SerialLink;

//Motor Variables:
Servo Motor;
int MotorPin = 13;
//Sensor Variables

int Emergency = -1;
int Idle = 53;
int Floor5 = A0;
int Floor4 = A1;
int Floor3 = A2;
int Floor2 = A3;
int Floor1 = A4;

int MinLevel = Floor1;
int MaxLevel = Floor4;

//chenge Floor1 to -1 then create function to go up or down based on the button pressed
int CurrentFloor = Floor1;
int NextFloor = Idle;

void setup() {
  //initialise Serial and Motor
  SerialLink.init();
  Motor.attach(MotorPin);
  SerialLink.setAgreedValue(Idle);
  //set the motor toIdle/No Movement
  Motor.write(90);

}

void loop() {
  //check for incoming messages
  SerialLink.receiveMessage();
  //incoming message will be the state/destination
  NextFloor = SerialLink.getAgreedValue();
  //check sensor readings to find current location
   CheckSensors();
  //control motor to go to the state/destination
  MotorControl();
  //delay to stop spamming of logging messages(will reove when logging fixed
  //delay(1000);
}


void MotorControl() {
  //The Current floor(Sensor readings) and the destination(received Message) are the same
  // Serial.print("CurrentFloor");
  // Serial.println(CurrentFloor);
  // Serial.print("NextFloor");
  // Serial.println(NextFloor);

  if (CurrentFloor == NextFloor) {
    SerialLink.setAgreedValue(Idle);
    NextFloor = Idle;
    Serial.print("Destination Has Been Reached");
    return;
  }
  if ((NextFloor == 53 || NextFloor == -1) && Motor.read() != 90  ) {
    Serial.println("Motor Halted, Complete Stop");
    Motor.write(90);
    return;
  }

  //An emergency has been declared
  if (SerialLink.getAgreedValue() == -1) {
    Serial.println("Emergency Stop: Hard Reset Required");
    Motor.write(90);
    EmergencyStop();
  }
  //if destination is at a higher location than current location and is not already going up
  if (NextFloor < CurrentFloor && Motor.read() != 0 && NextFloor != Idle) {
    Serial.println("Motor Started, Going Up");
    Motor.write(0);
  }

  //if destination is at a lower location than current location and is not already going down
  if (NextFloor > CurrentFloor && Motor.read() != 180 && NextFloor != Idle) {
    Serial.println("Motor Started, Going Down");
    Motor.write(180);
  }
  //if the state is Idle / there is no destination and it is not already idle then hold the motor still


}

void CheckSensors() {
  /*
    If the sensor has been triggered AND the sensor wasnt the last
    sensor triggered, the set sensor to current floor

    if (analogRead(Floor1)  < 50 && CurrentFloor != Floor1) {
    Serial.println("Current Floor is Floor 1");
    CurrentFloor = Floor1;
        SerialLink.sendMessage(Floor1);

    //add sending commands here for current floor to be sent to uno
    }
  */
  if (analogRead(Floor2)  < 50 && CurrentFloor != Floor2) {
    Serial.println("Current Floor is Floor 2");
    CurrentFloor = Floor2;
    SerialLink.sendMessage(Floor2);
  }
  if (analogRead(Floor3)  < 50 && CurrentFloor != Floor3) {
    Serial.println("Current Floor is Floor 3");
    CurrentFloor = Floor3;
    SerialLink.sendMessage(Floor3);
  }
  if (analogRead(Floor4)  < 50 && CurrentFloor != Floor4) {
    Serial.println("Current Floor is Floor 4");
    CurrentFloor = Floor4;
    SerialLink.sendMessage(Floor4);
  }
  if (analogRead(Floor5)  < 50 && CurrentFloor != Floor5) {
    Serial.println("Current Floor is Floor 5");
    CurrentFloor = Floor5;
    SerialLink.sendMessage(Floor5);
  }
  //  SerialLink.sendMessage(CurrentFloor);

}


//Emergency State, can only leave with hard reset
void EmergencyStop() {
  delay(10000);
  EmergencyStop();
}
