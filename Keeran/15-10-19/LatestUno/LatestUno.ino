#include "UserInterface.hpp"
#include <SoftwareSerial.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

//Agreed Values
//AgreedValue is value shared between C20 and C10 for designated instructions
int Emergency = -1;
int Idle = 53;
int Floor5 = 54;
int Floor4 = 59;
int Floor3 = 56;
int Floor2 = 57;
int Floor1 = 58;
int CurrentFloor = -1;
int AgreedValue = 0;

//Pin Allocation
// pin A5 and A4 are being used for i2c
int RX = 10;
int TX = 11;
int InterruptPin = 2;
int PinEmergencyButton = 9;
int PinLEDEmergency = A0;
int PinButtonFloor1 = 4;
int PinButtonFloor2 = 5;
int PinButtonFloor3 = 6;
int PinButtonFloor4 = 7;
int PinButtonFloor5 = 8;
int PinLEDFloor1 = 3;//maybe send back to pin 13, see what m50 say
int PinLEDFloor2 = 12;
int PinLEDFloor3 = A3;
int PinLEDFloor4 = A2;
int PinLEDFloor5 = A1;

// Serial Connection Baud Rate between Arduinos
long ArduinoComBaud = 9600;
//Serial Connection Baud Rate Between PC and Connected Arduino
long PCBuad = 115200;
//Logging to print statements to serial monitor between PC and Arduino
boolean Logging = false;
String LoggingDivider = "------------------------------------------";

//board and component initialisation
LiquidCrystal_I2C lcd(0x27, 20, 4);
SoftwareSerial SoftSerial(RX, TX);
UserInterface Floor1Button(PinButtonFloor1, PinLEDFloor1);
UserInterface Floor2Button(PinButtonFloor2, PinLEDFloor2);
UserInterface Floor3Button(PinButtonFloor3, PinLEDFloor3);
UserInterface Floor4Button(PinButtonFloor4, PinLEDFloor4);
UserInterface Floor5Button(PinButtonFloor5, PinLEDFloor5);
UserInterface EmergencyButton(PinEmergencyButton, PinLEDEmergency);


void setup() {
  lcdSetup();

  //initialising Serial Comms/Data Logging
  Serial.begin(PCBuad);
  SoftSerial.begin(ArduinoComBaud);

  //Initialising Buttons and Interrupt
  Floor1Button.init();
  Floor2Button.init();
  Floor3Button.init();
  Floor4Button.init();
  Floor5Button.init();
  EmergencyButton.init();

  //Printing Board And File Information
  Serial.println("Current Board: "); Serial.println("ARDUINO_AVR_UNO");
  Serial.print("Sketch:   ");   Serial.println(__FILE__);
  Serial.print("Uploaded: ");   Serial.println(__DATE__);
  Serial.println(LoggingDivider);
  Serial.println("Serial Initialised");
  Serial.print("RX Pin: ");    Serial.println(RX);
  Serial.print("TX Pin: ");    Serial.println(TX);
  Serial.println(LoggingDivider);

  //Setting the state to Idle by Default
  setAgreedValue(Idle);

  //interrupt for the buttons
  attachInterrupt(digitalPinToInterrupt(InterruptPin), ButtonPressInterrupt, RISING);


}

void loop() {
  receiveMessage();
  lcdDisplay();
}



void lcdSetup() {
  lcd.init();
  lcd.init();
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print("Current: ");
  lcd.setCursor(1, 1);
  lcd.print("Next: ");
}

void lcdDisplay() {
  int Current = 0;
  int next = 0;


  if (Current == Floor5) {
    Current = 5;
  } else if (Current == Floor4) {
    Current = 4;
  } else if (Current == Floor3) {
    Current = 3;
  } else if (Current == Floor2) {
    Current = 2;
  } else if (Current == Floor1) {
    Current = 1;
  } else {
    Current = 0;
  }

  if (AgreedValue == Floor5) {
    next = 5;
  } else if (AgreedValue == Floor4) {
    next = 4;
  } else if (AgreedValue == Floor3) {
    next = 3;
  } else if (AgreedValue == Floor2) {
    next = 2;
  } else if (AgreedValue == Floor1) {
    next = 1;
  } else {
    next = 0;
  }

  lcd.setCursor(10, 0);
  lcd.print("Floor");
  lcd.setCursor(15, 0);
  lcd.print(Current);
  lcd.setCursor(10, 1);
  lcd.print("Floor");
  lcd.setCursor(15, 1);
  lcd.print(next);
}
void receiveMessage() {
  if (SoftSerial.available()) {
    char val = SoftSerial.read();
    int MessageInt = val;
    //received an Emergency
    if (val == 'E') {
      Serial.println("Emergency Stop: Hard Reset Required");
      DeactivateAllButtons();
      setAgreedValue(Emergency);
      EmergencyStop();
      Serial.println(LoggingDivider);

    }
    //Floor Has been reached or lift has reset
    if (MessageInt == Idle) {
      Serial.println("Received Message: Idle");
      DeactivateAllButtons();
      setAgreedValue(Idle);
      Serial.println(LoggingDivider);

    }
    //record the floor it is currently on
    if (MessageInt == Floor5 || MessageInt == Floor4 || MessageInt == Floor3 || MessageInt == Floor2 || MessageInt == Floor1) {
      CurrentFloor = val;
      Serial.println("Received Message: Current Floor Change");
      Serial.println(LoggingDivider);

    }
  }
  if (CurrentFloor == AgreedValue) {
    DeactivateAllButtons();
    setAgreedValue(Idle);
  }
}


void sendMessage(int Message) {
  if (Message == Emergency) {
    SoftSerial.println('E');
  }
  if (Message == Idle) {
    SoftSerial.println(Idle);
  }
  if (Message == Floor5) {
    SoftSerial.println(Floor5);
  }
  if (Message == Floor4) {
    SoftSerial.println(Floor4);
  }
  if (Message == Floor3) {
    SoftSerial.println(Floor3);
  }
  if (Message == Floor2) {
    SoftSerial.println(Floor2);
  }
  if (Message == Floor1) {
    SoftSerial.println(Floor1);
  }
  Serial.print("Message Sent: "); Serial.println(Message);
  Serial.println(LoggingDivider);
}

int getAgreedValue() {
  return AgreedValue;
  Serial.print("Agreed Value Retreived: "); Serial.println(AgreedValue);
  Serial.println(LoggingDivider);
}


//only one button can be pressed at a time, it checks from floor one upwords
void ButtonPressInterrupt() {
  if (EmergencyButton.checkPressed()) {

    Serial.println("Emergency Stop: Hard Reset Required");
    DeactivateAllButtons();
    EmergencyButton.activateState();
    setAgreedValue(Emergency);
    CurrentFloor = -1;

    EmergencyStop();
  }
  if (getAgreedValue() != Idle) {
    return;
  }
  if (Floor1Button.checkPressed()) {
    Serial.println("Floor 1 Pressed");
    setAgreedValue(Floor1);
    return;
  } else if (Floor2Button.checkPressed()) {
    Serial.println("Floor 2 Pressed");
    setAgreedValue(Floor2);
    return;
  } else if (Floor3Button.checkPressed()) {
    Serial.println("Floor 3 Pressed");
    setAgreedValue(Floor3);
    return;
  } else if (Floor4Button.checkPressed()) {
    Serial.println("Floor 4 Pressed");
    setAgreedValue(Floor4);
    return;
  } else if (Floor5Button.checkPressed()) {
    Serial.println("Floor 5 Pressed");
    setAgreedValue(Floor5);
    return;
  }
}
void setAgreedValue(int value) {
  if (value == Emergency || value == Idle || value == Floor5 || value == Floor4 || value == Floor3 || value == Floor2 || value == Floor1) {
    AgreedValue = value;
  } else {
    AgreedValue = -1;
  }

  sendMessage(AgreedValue);

  Serial.print("Agreed Value Set To: "); Serial.println(AgreedValue);
  Serial.println(LoggingDivider);

}
void DeactivateAllButtons() {
  Floor1Button.deactivateState();
  Floor2Button.deactivateState();
  Floor3Button.deactivateState();
  Floor4Button.deactivateState();
  Floor5Button.deactivateState();
  EmergencyButton.deactivateState();
}
void EmergencyStop() {
  delay(10000);
  EmergencyStop();
}
