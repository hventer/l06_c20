#include "Arduino.h"
#include "UserInterface.hpp"

UserInterface::UserInterface(int pinButton, int pinLED) {
  //pins
  this->pinButton = pinButton;
  this->pinLED = pinLED;

  this->ButtonState = 0;
}

void UserInterface::init() {
  //pin modes
  pinMode(pinLED, OUTPUT);
  pinMode(pinButton, INPUT);

  //set both to low initially
  digitalWrite(pinButton, LOW);
  digitalWrite(pinLED, LOW);
}

bool UserInterface::checkPressed() {
  //Has it been pressed or is it being pressed
  ButtonState = digitalRead(pinButton);
  if (ButtonState == 1) {
    activateState();
    return true;
  } else {
    return false;
  }
}
boolean UserInterface::getState() {
  return this->state;
}


void UserInterface::activateState() {
  this->state = true;
  digitalWrite(pinLED, HIGH);
}

void UserInterface::deactivateState() {
  this->state = false;
  digitalWrite(pinLED, LOW);
}

void UserInterface::doublePress() {
  //TBI
}


