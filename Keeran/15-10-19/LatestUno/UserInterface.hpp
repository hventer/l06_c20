#ifndef UserInterface_HPP
#define UserInterface_HPP

#include "Arduino.h"

class UserInterface {
  private:
    boolean state = false;
    int pinButton;
    int pinLED;
    int ButtonState;

  public:
    UserInterface(int pinButton, int pinLED);
    void init();
    bool checkPressed();
    void deactivateState();
    void activateState();
    void doublePress();
    boolean getState();

};

#endif
