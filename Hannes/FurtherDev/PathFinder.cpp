#include "Arduino.h"
#include "PathFinder.hpp"
#include <StackArray.h>

PathFinder::PathFinder() {
    this->direction = 1;
}

char PathFinder::getLevelDown() {
    //return next level
    return down.pop()
}

void PathFinder::addLevelDown(char level) {
    down.push(level);
    down.sort(); //sort only does ascending
}

char PathFinder::getLevelUp() {
    //return next level
    return level.pop()
}

void PathFinder::addLevelUp(char level) {
    up.push(level);
    up.sort(); //sort ascending
}

char PathFinder::getLevel(char megaLevel) {
    if(direction && up.isEmpty()) {
        direction = 0;
    }
    if(!direction && down.isEmpty()) {
        direction = 1;
    }
    if(!direction && down.peek() < megaLevel && !down.isEmpty()) {
        return getLevelUp();
    }
    else if(direction && up.peek() > megaLevel && !up.isEmpty()) {
        return getLevelDown();
    }
    return 0;
}

void PathFinder::addLevel(char level, char megaLevel) {
    if(level > megaLevel) {
        addLevelUp(level);
    }
    else if(level < megaLevel) {
        addLevelDown(level);
    }
    else {
        //current level, do nothing
    } 
}

void PathFinder::remove(char level) {
    up.remove(level);
    down.remove(level);
}
