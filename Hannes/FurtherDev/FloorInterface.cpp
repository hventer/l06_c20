#include "Arduino.h"
#include "FloorInterface.hpp"

FloorInterface::FloorInterface(char pinButton, char pinLED, char floor) {
    //pins
    this->pinButton = pinButton;
    this->pinLED = pinLED;
    this->floor = floor;

    //button states
    this->curButtonState = 0;
    this->preButtonState = 0;

    //currently active
    this->curActive = 0;
    //was this previously active?
    this->preActive = 0;
}

void FloorInterface::init() {
    //pin modes
    pinMode(pinLED, OUTPUT);
    pinMode(pinButton, INPUT_PULLUP);

    //set both to low initially
    digitalWrite(pinButton, HIGH);
    digitalWrite(pinLED, LOW);
}

char FloorInterface::getLevel() {
    return this->floor;
}

void FloorInterface::checkPressed() {
    preButtonState = curButtonState;
    curButtonState = digitalRead(pinButton);

    //if any button pressed do action
    if(preButtonState == 1 && curButtonState == 0) {
        activateLED();
        this->curActive = 1;
    }
}

void FloorInterface::activateLED() {
    digitalWrite(pinLED, HIGH);
}

void FloorInterface::deactivateLED() {
    digitalWrite(pinLED, LOW);
}

void FloorInterface::deactivate() {
    deactivateLED();
    this->curActive = 0;
    this->preActive = 0;
}

char FloorInterface::isNewActive() {
    //is this currently active and not previously active
    if(this->curActive && !this->preActive) {
      this->preActive = 1; //not newly active anymore
      return 1;
    }
    else
      return 0;
}
