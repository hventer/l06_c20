#include "Arduino.h"
#include "EmergencyInterface.hpp"

EmergencyInterface::EmergencyInterface(int pinButton, int pinLED) {
    //pins
    this->pinButton = pinButton;
    this->pinLED = pinLED;

    //button states
    this->curButtonState = 0;
    this->preButtonState = 0;

}

void EmergencyInterface::init() {
  //pin modes
    pinMode(pinLED, OUTPUT);
    pinMode(pinButton, INPUT_PULLUP);

    //set both to low initially
    digitalWrite(pinButton, HIGH);
    digitalWrite(pinLED, LOW);
}

bool EmergencyInterface::checkPressed() {
    preButtonState = curButtonState;
    curButtonState = digitalRead(pinButton);

      //if any button pressed do action
  if (preButtonState == 1 && curButtonState == 0) {
    activateLED();
    return true;
  }
  else {
    return false;
  }
}

void EmergencyInterface::activateLED() {
    digitalWrite(pinLED, HIGH);
}

void EmergencyInterface::deactivateLED() {
    digitalWrite(pinLED, LOW);
}
