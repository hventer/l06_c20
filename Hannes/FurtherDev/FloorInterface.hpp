#ifndef FLOORINTERFACE_HPP
#define FLOORINTERFACE_HPP

#include "Arduino.h"

class FloorInterface {
    private:
        char pinButton;
        char pinLED;
        char floor;
        char curButtonState;
        char preButtonState;
        char curActive;
        char preActive;
        void deactivateLED();
        void activateLED();



    public:
        FloorInterface(char pinButton, char pinLED, char floor);
        void init();
        char getLevel();
        void checkPressed();
        void deactivate();
        char isNewActive();
};

#endif
