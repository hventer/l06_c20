#include "FloorInterface.hpp"
#include "EmergencyInterface.hpp"
#include "PathFinder.hpp"
#include <SoftwareSerial.h>

#define PIN_BUTTON_EMERGENCY 4
#define PIN_LED_EMERGENCY A0
#define PIN_BUTTON_1 5
#define PIN_LED_1 A1
#define PIN_BUTTON_2 6
#define PIN_LED_2 A2
#define PIN_BUTTON_3 7
#define PIN_LED_3 A3
#define PIN_BUTTON_4 8
#define PIN_LED_4 A4
#define PIN_BUTTON_5 9
#define PIN_LED_5 A5
#define RX 2
#define TX 3
#define DATA_RATE 4800

EmergencyInterface em(PIN_BUTTON_EMERGENCY, PIN_LED_EMERGENCY);
FloorInterface lvl_1(PIN_BUTTON_1, PIN_LED_1, 1);
FloorInterface lvl_2(PIN_BUTTON_2, PIN_LED_2, 2);
FloorInterface lvl_3(PIN_BUTTON_3, PIN_LED_3, 3);
FloorInterface lvl_4(PIN_BUTTON_4, PIN_LED_4, 4);
FloorInterface lvl_5(PIN_BUTTON_5, PIN_LED_5, 5);
SoftwareSerial megaSerial(RX, TX);

//current/previous level mega was on
char megaLevel = 0;

//0 mean moving
//1 means stationary
char megaState = 0;

PathFinder path = new PathFinder();


void setup() {
  initiateSerialConnection();
  Serial.begin(9600);
  initiateButtons();
}

void loop() {
  checkButtons();
  readSerial();
  sortButtons();
  detectEmergency();

  if(megaState) { //waiting
    char nextLevel = path.getLevel(megaLevel);
    if(nextLevel) { //if it returned a level
      megaSerial.write(nextLevel);
      megaState = 0;
    }
  }
}

void checkButtons() {
  lvl_1.checkPressed();
  lvl_2.checkPressed();
  lvl_3.checkPressed();
  lvl_4.checkPressed();
  lvl_5.checkPressed();
  detectEmergency();
}

void readSerial() {
  // read what the MEGA has sent
  if(megaSerial.available()) {
    Serial.println(megaSerial.read());
    megaLevel = megaSerial.read();
    deactivateButton(megaLevel);
    megaState = 1; //they're waiting
  }
}

void deactivateButton(char level) {
  if(level == 1) {
    lvl_1.deactivate();
    path.remove(lvl_1.getLevel())
  }
  else if(level == 2) {
    lvl_2.deactivate();
    path.remove(lvl_2.getLevel())
  }
  else if(level == 3) {
    lvl_3.deactivate();
    path.remove(lvl_3.getLevel())
  }
  else if(level == 4) {
    lvl_4.deactivate();
    path.remove(lvl_4.getLevel())
  }
  else if(level == 5) {
    lvl_5.deactivate();
    path.remove(lvl_5.getLevel())
  }
}

void sortButtons() {
  if(lvl_1.isNewActive())
    path.addLevel(lvl_1.getLevel(), megaLevel);
    
  if(lvl_2.isNewActive())
    path.addLevel(lvl_2.getLevel(), megaLevel);
    
  if(lvl_3.isNewActive())
    path.addLevel(lvl_3.getLevel(), megaLevel);
    
  if(lvl_4.isNewActive())
    path.addLevel(lvl_4.getLevel(), megaLevel);
      
  if(lvl_5.isNewActive())
    path.addLevel(lvl_5.getLevel(), megaLevel);
}

void detectEmergency() {
  if(em.checkPressed()) {
    megaSerial.write(-1);
    decativateButton(lvl_1.getLevel());
    decativateButton(lvl_2.getLevel());
    decativateButton(lvl_3.getLevel());
    decativateButton(lvl_4.getLevel());
    decativateButton(lvl_5.getLevel());
    megaState = 0;
  }
}

void initiateSerialConnection() {
  pinMode(RX, INPUT);
  pinMode(TX, OUTPUT);
  // set the data rate for the SoftwareSerial port
  megaSerial.begin(DATA_RATE);
}

void initiateButtons() {
  lvl_1.init();
  lvl_2.init();
  lvl_3.init();
  lvl_4.init();
  lvl_5.init();
}
