#ifndef PATHFINDER_HPP
#define PATHFINDER_HPP

#include "Arduino.h"
#include <StackArray.h>


class PathFinder {
    private:
        StackArray<char> down, up;
        char direction;
        char getLevelDown();
        char getLevelUp();
        void addLevelDown(char level);
        void addLevelUp(char level);
        void sort(char dir);

    public:
        PathFinder();
        char getLevel(char megaLevel);
        void addLevel(char level, char megaLevel);
        void remove(char level);
};

#endif
