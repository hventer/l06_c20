/*
  Button

  Turns on and off a light emitting diode(LED) connected to digital pin 13,
  when pressing a pushbutton attached to pin 2.

  The circuit:
  - LED attached from pin 13 to ground
  - pushbutton attached to pin 2 from +5V
  - 10K resistor attached to pin 2 from ground

  - Note: on most Arduinos there is already an LED on the board
    attached to pin 13.

  created 2005
  by DojoDave <http://www.0j0.org>
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Button
*/

// constants won't change. They're used here to set pin numbers:
const int buttonPin1 = 2;     // the number of the pushbutton pin
const int ledPin1 =  11;      // the number of the LED pin
const int buttonPin2 = 3;     // the number of the pushbutton pin
const int ledPin2 =  12;      // the number of the LED pin
const int buttonPin3 = 4;     // the number of the pushbutton pin
const int ledPin3 =  13;      // the number of the LED pin


// variables will change:
int buttonState1 = 0;         // variable for reading the pushbutton status
int buttonState2 = 0;         // variable for reading the pushbutton status
int buttonState3 = 0;         // variable for reading the pushbutton status


void setup() {
  Serial.begin(9600);

  // initialize the LED pin as an output:
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);

  // initialize the pushbutton pin as an input:
  pinMode(buttonPin1, INPUT_PULLUP);
  pinMode(buttonPin2, INPUT_PULLUP);
  pinMode(buttonPin3, INPUT_PULLUP);

}

void loop() {
  // read the state of the pushbutton value:

  buttonState1 = digitalRead(buttonPin1);
  buttonState2 = digitalRead(buttonPin2);
  buttonState3 = digitalRead(buttonPin3);


  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState1 == LOW) {
    // turn LED on:
    digitalWrite(ledPin1, HIGH);
    Serial.println("Button 1");
    buttonState1 = 0;
  } else {
    // turn LED off:
    digitalWrite(ledPin1, LOW);
    buttonState1 = 0;
  }

  if (buttonState2 == LOW) {
    // turn LED on:
    digitalWrite(ledPin2, HIGH);
    Serial.println("Button 2");
  } else {
    // turn LED off:
    digitalWrite(ledPin2, LOW);
    buttonState2 = 0;

  }

  if (buttonState3 == LOW) {
    // turn LED on:
    digitalWrite(ledPin3, HIGH);
    Serial.println("Button 3");

  } else {
    // turn LED off:
    digitalWrite(ledPin3, LOW);
    buttonState3 = 0;

  }
}
