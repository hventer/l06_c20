#ifndef MOTOR_HPP
#define MOTOR_HPP

#include "Arduino.h"

class Motor {
    private:
        int pin;
        int buttonState;
        int LEDState;

    public:
        Motor(int pin);
        void SetSpeed(float speed);
        void stop();
};

#endif