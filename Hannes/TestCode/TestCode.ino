#include "Motor.hpp"
#define MOTOR1_PIN 1

Motor motor1;

void setup() {
  // put your setup code here, to run once:
  Motor motor1(MOTOR1_PIN);

}

void loop() {
  // put your main code here, to run repeatedly:
  motor1.setSpeed(1);
  motor1.stop();
}
