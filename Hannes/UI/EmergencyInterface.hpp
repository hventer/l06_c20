#ifndef EMERGENCYINTERFACE_HPP
#define EMERGENCYINTERFACE_HPP

#include "Arduino.h"

class EmergencyInterface {
    private:
        int pinButton;
        int pinLED;
        int curButtonState;
        int preButtonState;

    public:
        EmergencyInterface(int pinButton, int pinLED);
        void init();
        bool checkPressed();
        void activateLED();
        void deactivateLED();
};

#endif