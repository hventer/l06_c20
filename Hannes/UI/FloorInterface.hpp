#ifndef FLOORINTERFACE_HPP
#define FLOORINTERFACE_HPP

#include "Arduino.h"

class FloorInterface {
    private:
        char pinButton;
        char pinLED;
        char floor;
        char curButtonState;
        char preButtonState;
        char active;


    public:
        FloorInterface(char pinButton, char pinLED, char floor);
        void init();
        char getLevel();
        void checkPressed();
        void activateLED();
        void deactivateLED();
        void deactivate();
        bool isActive();
};

#endif
