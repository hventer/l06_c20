#include "FloorInterface.hpp"
#include "EmergencyInterface.hpp"
#include <SoftwareSerial.h>

#define PIN_BUTTON_UP 5
#define PIN_LED_UP 12
#define PIN_BUTTON_DOWN 4
#define PIN_LED_DOWN 11
#define PIN_BUTTON_EMERGENCY 6
#define PIN_LED_EMERGENCY 10
#define RX 2
#define TX 3
#define DATA_RATE 4800

FloorInterface down(PIN_BUTTON_DOWN, PIN_LED_DOWN, 1);
FloorInterface up(PIN_BUTTON_UP, PIN_LED_UP, 2);
EmergencyInterface em(PIN_BUTTON_EMERGENCY, PIN_LED_EMERGENCY);
SoftwareSerial megaSerial(RX, TX); // RX, TX

//start this as true
char megaState = 0;

void setup() {
  pinMode(RX, INPUT);
  pinMode(TX, OUTPUT);
  Serial.begin(9600);
  // set the data rate for the SoftwareSerial port
  megaSerial.begin(DATA_RATE);

  down.init();
  up.init();
  em.init();
}

void loop() {

  if(megaState) {
    down.deactivate();
    up.deactivate();
    
    down.checkPressed();
    if(down.isActive()) {
      megaSerial.write(down.getLevel());
      megaState = 0;
    }
    else {
      up.checkPressed();
      if(up.isActive()) {
        megaSerial.write(up.getLevel());
        megaState = 0;
      }
    }
  }

  

  // read what the MEGA has sent
  if(megaSerial.available()) {
    Serial.println(megaSerial.read());
    megaState = megaSerial.read();
  }
 


/*
  if(em.checkPressed()) {
    Serial.println("EMERGENCY PRESSED");
    down.deactivate();
    up.deactivate();
  } */
}
